from numpy import mean, std


class Report:
    def __init__(self, name, cv_score, pred_score, clf):
        self.name = name
        self.cv_score = cv_score
        self.pred_score = pred_score
        self.clf = clf
        
    def cv_score_format(self):
        cv_score = self.cv_score
        print(
            'Cross-Validation evaluation:\n'
            'accuracy       avg:%.3f    std:%.3f\n'
            'f1-score       avg:%.3f    std:%.3f\n'
            'recall         avg:%.3f    std:%.3f\n'
            'precision      avg:%.3f    std:%.3f\n' % (
                                    mean(cv_score['test_accuracy']), std(cv_score['test_accuracy']),
                                    mean(cv_score['test_f1_macro']), std(cv_score['test_f1_macro']),
                                    mean(cv_score['test_recall_macro']), std(cv_score['test_recall_macro']),
                                    mean(cv_score['test_precision_macro']), std(cv_score['test_precision_macro'])))

    def pred_score_format(self):
        print('Prediction evaluation:\n')
        print(self.pred_score)

    def metrics(self, source):
        cv_score = self.cv_score
        return f'%s;%s;%.3f;%.3f;%.3f;%.3f;%.3f;%.3f;%.3f;%.3f;%.3f\n' % (
                                    source, self.name,
                                    mean(cv_score['test_accuracy']), std(cv_score['test_accuracy']),
                                    mean(cv_score['test_f1_macro']), std(cv_score['test_f1_macro']),
                                    mean(cv_score['test_recall_macro']), std(cv_score['test_recall_macro']),
                                    mean(cv_score['test_precision_macro']), std(cv_score['test_precision_macro']), self.pred_score['accuracy'])