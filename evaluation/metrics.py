from numpy import mean, std
import numpy
from sklearn.model_selection import StratifiedKFold, cross_validate
from sklearn.metrics import classification_report
import keras.backend as K

# Realiza o treinamento e avaliação de modelos Sklearn utilizado a técnica de Cross-Validation 10folds. 
# São retornadas as métricas do treinamento e de predição.
def train_and_evaluate(model, X_train, X_test, y_train, y_test):
    cv_score = cross_validation_score(model, X_train, y_train, cv=10)
    model.fit(X_train, y_train)
    pred_score = predict_score(model, X_test, y_test)
    return tuple((cv_score, pred_score))

# Realiza o treinamento e avaliação de modelos Keras (redes neurais recorrentes) utilizado a técnica de Cross-Validation 10folds. 
# São retornadas as métricas do treinamento e de predição.
def train_and_evaluate_rnn(build_model_fn, X_train_padded, X_test_padded, y_train, y_test, num_words, num_epochs=5, cv=10):
    # separa os dados em pastas, embaralhando os dados
    kfold = StratifiedKFold(n_splits=cv, shuffle=True, random_state=numpy.random.seed(42))
    #variáveis para salvar métricas de cada pasta
    cvs_acc_scores = []
    cvs_precision_scores = []
    cvs_recall_scores = []
    cvs_acc_scores = []
    cvs_f1_scores = []

    model = None

    for train_index, test_index in kfold.split(X_train_padded, y_train):
        fx_train, fx_test = X_train_padded[train_index], X_train_padded[test_index]
        fy_train, fy_test = y_train[train_index], y_train[test_index]

        model = build_model_fn(num_words)
        model.fit(fx_train, fy_train, epochs=num_epochs)
        #métricas de treinamento
        score, accuracy, precision, recall, f1_score = model.evaluate(fx_test, fy_test, verbose=0)
        cvs_acc_scores.append(accuracy)
        cvs_precision_scores.append(precision)
        cvs_recall_scores.append(recall)
        cvs_f1_scores.append(f1_score)

    cv_score = {
        'test_accuracy': cvs_acc_scores,
        'test_recall_macro': cvs_recall_scores,
        'test_precision_macro': cvs_precision_scores,
        'test_f1_macro': cvs_f1_scores
    }
    #avalia as predições com os dados de teste
    pred_score = predict_score(model, X_test_padded, y_test, rnn=True)

    #Keras deve limpar a sessão ao final. Caso contrário influenciará na criação de um novo modelo, pois o Keras armazena o estado de camadas em memória.
    K.clear_session()

    return tuple((cv_score, pred_score, model))

# aplicação da técnica de treinamento Cross-Validation 10folds. Retorna as métricas do treinamento.
def cross_validation_score(model, X_train, y_train, cv=10):
    return cross_validate(model, X_train, y_train, cv=cv,
                       scoring=['accuracy', 'f1_macro', 'recall_macro', 'precision_macro'])

# avalia o modelo treinado com os dados de teste e retorna um relatório. Caso seja uma rede neural Keras, utilize rnn=True
def predict_score(model, X_test, y_test, rnn=False):
    y_pred = model.predict(X_test)
    if rnn:
        y_pred_bool = (y_pred > 0.5).astype("int32")
        return classification_report(y_test, y_pred_bool, output_dict=True)
    else:
        return classification_report(y_test, y_pred, output_dict=True)

#Keras não disponibiliza a métrica F1-Score, portanto é necessário implementar manualmente:
# Implementação de https://aakashgoel12.medium.com/how-to-add-user-defined-function-get-f1-score-in-keras-metrics-3013f979ce0d
def get_f1(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    recall = true_positives / (possible_positives + K.epsilon())
    f1_val = 2*(precision*recall)/(precision+recall+K.epsilon())
    return f1_val