

from analysis.cloud_word import cloud_words_overall, cloud_words_steam, cloud_words_twitter
from analysis.sentiment_analysis import sentiment_analysis
from analysis.test_stacking_combinations import test_stacking_combinations
from dotenv import load_dotenv

if __name__ == "__main__":
    load_dotenv()

    #test_stacking_combinations()

    #sentiment_analysis()
    
    #cloud_words_twitter()
    #cloud_words_steam()