from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import ComplementNB, MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from algorithm.supervised.lstm import build_model_lstm

models = {
    'CNB':   ComplementNB(),
    'MNB':  MultinomialNB(),
    'SVM':  SVC(),
    'AD':   DecisionTreeClassifier(),
    'FA':   RandomForestClassifier(),
    'RL':   LogisticRegression(multi_class='ovr', max_iter=200)
}