
from keras.layers.core import Dense, SpatialDropout1D
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.models import Sequential

from algorithm.supervised.neural_network import neural_network
from evaluation.metrics import get_f1
from model.report import Report

import tensorflow as tf
import nltk
nltk.download('punkt')

def lstm(X_train, X_test, y_train, y_test):
    print('Training and evaluating LSTM\n')
    cv_score, pred_score, clf = neural_network(build_model_lstm, X_train, X_test, y_train, y_test)
    return Report('LSTM', cv_score, pred_score, clf)

def build_model_lstm(num_words, hidden_layer_size=32, embedding_size=64, max_sentence_length=900):
    model = Sequential()
    model.add(Embedding(num_words, embedding_size, input_length=max_sentence_length))
    model.add(SpatialDropout1D((0.2)))
    model.add(LSTM(hidden_layer_size, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall(), get_f1])
    model.summary()

    return model
    