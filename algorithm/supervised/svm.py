
from sklearn.svm import SVC
from evaluation.metrics import  train_and_evaluate
from model.report import Report

def svm(X_train, X_test, y_train, y_test):
    print('Training and evaluating SVM\n')
    clf = SVC()
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('SVM', cv_score, pred_score, clf)
