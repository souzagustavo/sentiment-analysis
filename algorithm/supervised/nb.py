from sklearn.naive_bayes import ComplementNB
from evaluation.metrics import train_and_evaluate
from model.report import Report

def nb(X_train, X_test, y_train, y_test):
    print('Training and evaluating NB\n')
    clf = ComplementNB()
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('CNB', cv_score, pred_score, clf)