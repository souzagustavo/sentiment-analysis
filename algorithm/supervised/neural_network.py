import collections

from keras.preprocessing.sequence import pad_sequences
from evaluation.metrics import train_and_evaluate_rnn

import nltk
import numpy
nltk.download('punkt')

def frequency(docs, max_features=10000):
    maxlen = 0
    word_freqs = collections.Counter()
    num_recs = 0

    for i in docs:
        words = nltk.word_tokenize(i.lower())
        if len(words) > maxlen:
            maxlen = len(words)
        for word in words:
            word_freqs[word] += 1
        num_recs += 1
    
    vocab_size = min(max_features, len(word_freqs)) + 2
    word2index = {x[0]: i+2 for i, x in enumerate(word_freqs.most_common(max_features))}
    word2index["PAD"] = 0
    word2index["UNK"] = 1
    return word2index, vocab_size

def extract(word2index, docs, classes, max_sentence_length=900):
    num_recs = len(docs)
    index2word = {v:k for k, v in word2index.items()}

    X = numpy.empty((num_recs, ), dtype=list)
    y = numpy.zeros((num_recs, ))
    i = 0

    for i in range(len(docs)):
        words = nltk.word_tokenize(docs[i].lower())
        seqs = []
        for word in words:

            if word in word2index.keys():
                seqs.append(word2index[word])
            else:
                seqs.append(word2index["UNK"])
        X[i] = seqs
        y[i] = int(classes[i])

    X = pad_sequences(X, maxlen=max_sentence_length)
    return X
    
def neural_network(build_model_fn, X_train, X_test, y_train, y_test):
    y_train = numpy.array(y_train)
    y_test = numpy.array(y_test)

    word2index, vocab_size = frequency(X_train+X_test)
    X_train = extract(word2index,X_train, y_train)
    X_test = extract(word2index, X_test, y_test)

    return train_and_evaluate_rnn(build_model_fn, X_train, X_test, y_train, y_test, vocab_size)