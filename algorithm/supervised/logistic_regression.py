

from sklearn.linear_model import LogisticRegression
from evaluation.metrics import train_and_evaluate
from model.report import Report

# Deve-se utilizar multi_class=ovr para problemas binários.
# max_iter=200 é o limite de iterações para convergência. default=100 (Testar numero de iterações)
def logistic_regression(X_train, X_test, y_train, y_test, max_iter=100):
    print('Training and evaluating LR\n')
    clf = LogisticRegression(multi_class='ovr', max_iter=max_iter) 
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('RL', cv_score, pred_score, clf)
    