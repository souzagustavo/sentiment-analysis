
from sklearn.tree import DecisionTreeClassifier
from evaluation.metrics import train_and_evaluate
from model.report import Report

def decision_tree(X_train, X_test, y_train, y_test):
    print('Training and evaluating DT\n')
    clf = DecisionTreeClassifier()
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('AD', cv_score, pred_score, clf)