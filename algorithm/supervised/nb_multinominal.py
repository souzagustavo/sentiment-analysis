from sklearn.naive_bayes import MultinomialNB
from evaluation.metrics import train_and_evaluate
from file_util.dump import save_model
from model.report import Report


def multinominal_nb(X_train, X_test, y_train, y_test):
    print('Training and evaluating NB Multinomial\n')
    clf = MultinomialNB()
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('MNB', cv_score, pred_score, clf)
