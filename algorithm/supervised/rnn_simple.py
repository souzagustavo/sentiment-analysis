
from keras.layers.core import  Dense, SpatialDropout1D
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import SimpleRNN
from keras.models import Sequential

import tensorflow as tf

from algorithm.supervised.neural_network import neural_network
from evaluation.metrics import get_f1
from model.report import Report

def rnn_simple(X_train, X_test, y_train, y_test):
    print('Training and evaluating RNN Simple\n')
    cv_score, pred_score, clf = neural_network(build_model_rnn, X_train, X_test, y_train, y_test)
    return Report('RNR Simples', cv_score, pred_score, clf)

def build_model_rnn(num_words, hidden_layer_size=32, embedding_size=64, max_sentence_length=900):
    model = Sequential()
    model.add(Embedding(num_words, embedding_size, input_length=max_sentence_length))
    model.add(SpatialDropout1D((0.2)))
    model.add(SimpleRNN(hidden_layer_size, return_sequences=False))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall(), get_f1])
    model.summary()

    return model