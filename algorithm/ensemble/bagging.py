
from sklearn.ensemble import BaggingClassifier
from evaluation.metrics import train_and_evaluate
from algorithm.classifiers_dict import models
from model.report import Report

def bagging(model, X_train, X_test, y_train, y_test):
    print('Training and evaluating Bagging-{}\n'.format(model))
    clf = BaggingClassifier(models[model])
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('Bagging {}'.format(model), cv_score, pred_score, clf)

    