from algorithm.classifiers_dict import models
from sklearn.ensemble import StackingClassifier
from evaluation.metrics import train_and_evaluate
from model.report import Report

def level0(level0_models):
    level0 = list()
    for model in level0_models:
        level0.append((model, models[model]))
    return level0

def level1(level1_model):
    return models[level1_model]

def stacking(combination, X_train, X_test, y_train, y_test):
    lvl1 = combination.pop()

    print('Training and evaluating Stacking {}\n'.format(lvl1))
    clf = StackingClassifier(estimators=level0(combination), final_estimator=level1(lvl1), cv=10, stack_method='predict')
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('Stacking {}'.format(lvl1), cv_score, pred_score, clf)
