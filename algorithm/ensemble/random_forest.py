from sklearn.ensemble import RandomForestClassifier
from evaluation.metrics import train_and_evaluate
from model.report import Report

# n_estimators=100 número de arvores por padrão
def random_forest(X_train, X_test, y_train, y_test, n_estimators=100):
    print('Training and evaluating RF\n')
    clf = RandomForestClassifier(n_estimators=n_estimators)
    cv_score, pred_score = train_and_evaluate(clf, X_train, X_test, y_train, y_test)
    return Report('FA', cv_score, pred_score, clf)