from numpy import arange
from sklearn.feature_extraction.text import CountVectorizer

import collections
import nltk

# converte textos de treinamento para sua representação de frequência
def bow(train_texts, test_texts):
    vect = CountVectorizer()
    vect.fit(train_texts)
    #print(vect.vocabulary_)
    return vect.fit_transform(train_texts), vect.transform(test_texts)

def most_frequency(docs, max_features=10000):
    maxlen = 0
    word_freqs = collections.Counter()
    num_recs = 0

    for i in docs:
        words = nltk.word_tokenize(i.lower())
        if len(words) > maxlen:
            maxlen = len(words)
        for word in words:
            word_freqs[word] += 1
        num_recs += 1
    
    return word_freqs.most_common(max_features)