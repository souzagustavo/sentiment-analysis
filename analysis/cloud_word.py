from matplotlib import pyplot as plt
from wordcloud import WordCloud
from database.db import connect, findAllReviewsByPolarityDescription, findAllReviewsBySourceAndPolarityDescription

# cria uma nuvem para textos positivos, negativos e neutros considerando os textos mais frequentes
def cloud_words_overall():
    cloud_words(labels=['neutral', 'negative', 'positive'])

# cria uma nuvem para textos positivos, negativos e neutros para Twitter considerando os textos mais frequentes
def cloud_words_twitter():
    cloud_words(labels=['neutral', 'negative', 'positive'], source='Twitter')

# cria uma nuvem para textos positivos, negativos para Steam considerando os textos mais frequentes. Steam não possui textos neutros.
def cloud_words_steam():
    cloud_words(labels=['negative', 'positive'], source='Steam')

def cloud_words(labels, source=None):
    client = connect()
    reviews = []
    for label in labels:
        if source is not None:
            reviews = findAllReviewsBySourceAndPolarityDescription(client, source, label)
        else:
            reviews = findAllReviewsByPolarityDescription(client, label)
        text = " ".join(review.processedText for review in reviews)
        wc = new_cloud(text)
        show_cloud(wc)

def new_cloud(text):
    return WordCloud(stopwords=set(), collocations=False, background_color="white", max_words=75, prefer_horizontal=1, min_font_size=10, 
        normalize_plurals=False, color_func=lambda *args, **kwargs: (0,0,0)).generate(text)

def show_cloud(wc):
    plt.imshow(wc, interpolation='bilinear')
    plt.axis("off")
    plt.show()