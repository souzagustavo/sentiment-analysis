from asyncio import as_completed
from concurrent.futures import ThreadPoolExecutor
from itertools import permutations

import numpy
from algorithm.ensemble.stacking import stacking
from algorithm.feature.bag_of_words import bow
from analysis.procedures import get_balance_data, get_classes, handle_report, split_train_test
from database.db import connect, findAllTwitterReviews
from file_util.dump import create_report_folder, file_header

# fontes dos dados
twitter, steam = 'Twitter', 'Steam'
# algoritmos supervisionados (não incluido lstm e rnn)
algorithms = ['CNB', 'MNB', 'SVM', 'AD', 'RL', 'FA']
# lista todas as combinações de algoritmos possíveis
combinations = [ p for p in permutations(algorithms)]
# como são muitas combinações, separei em listas menores para poder rodar em paralelo
combinations_chunks = numpy.array_split(combinations, 144) #720/144=5 ou 720/72=10 (pacotes em paralelo)

# testa as 720 possíveis combinações dos 6 algoritmos com o algoritmo de Stacking.
def test_stacking_combinations():
    create_report_folder()
    
    client = connect()

    limit = get_balance_data(client)

    reviews_twitter = findAllTwitterReviews(client, limit)
    sentiment_analysis_stacking(reviews_twitter, twitter)

def sentiment_analysis_stacking(reviews, source):
   #separar dados treinamento e teste
    training_reviews, test_reviews = split_train_test(reviews, 0.9)
    #X-feature y-label
    X_train_text, X_test_text, y_train, y_test = get_classes(training_reviews, test_reviews)
    #aplicar seleção de feature Bag of words
    X_train, X_test = bow(X_train_text, X_test_text)

    file_header(source)

    # lista todas as combinações de algoritmos possíveis
    combinations = [ p for p in permutations(algorithms)]
    # como são muitas combinações, separei em listas menores para poder rodar em paralelo
    combinations_chunks = numpy.array_split(combinations, 144) #720/144=5 ou 720/72=10 (pacotes em paralelo)

    for combinations in combinations_chunks:
        with ThreadPoolExecutor() as executor:
            results = [executor.submit(stacking, list(combination), X_train, X_test, y_train, y_test) for combination in combinations]
            for f in as_completed(results):
                handle_report( f.result(), source)