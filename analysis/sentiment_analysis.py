from concurrent.futures import ThreadPoolExecutor, as_completed
from algorithm.ensemble.bagging import bagging
from algorithm.ensemble.random_forest import random_forest
from algorithm.ensemble.stacking import stacking

from algorithm.feature.bag_of_words import bow
from algorithm.supervised.decision_tree import decision_tree
from algorithm.supervised.logistic_regression import logistic_regression
from algorithm.supervised.lstm import lstm
from algorithm.supervised.nb import nb
from algorithm.supervised.nb_multinominal import multinominal_nb
from algorithm.supervised.rnn_simple import rnn_simple
from algorithm.supervised.svm import svm
from analysis.procedures import get_balance_data, get_classes, handle_report, split_train_test
from database.db import connect, findAllSteamReviews, findAllTwitterReviews
from file_util.dump import create_report_folder, file_header

# fontes dos dados
twitter, steam = 'Twitter', 'Steam'
# algoritmos supervisionados (não incluido lstm e rnn)
algorithms = ['CNB', 'MNB', 'SVM', 'AD', 'RL', 'FA']

stacking_classifiers = [
    ('CNB', 'MNB', 'SVM', 'AD', 'RL', 'FA'),
    ('MNB', 'SVM', 'AD', 'RL', 'FA', 'CNB'),
    ('SVM', 'AD', 'RL', 'FA', 'CNB', 'MNB'),
    ('AD', 'RL', 'FA', 'CNB', 'MNB', 'SVM'),
    ('RL', 'FA', 'CNB', 'MNB', 'SVM', 'AD'),
    ('FA', 'CNB', 'MNB', 'SVM', 'AD', 'RL'),
]

def sentiment_analysis(): 
    create_report_folder()
    
    client = connect()

    limit = get_balance_data(client)

    reviews_steam = findAllSteamReviews(client, limit)
    sentiment_analysis(reviews_steam, steam)

    reviews_twitter = findAllTwitterReviews(client, limit)
    sentiment_analysis(reviews_twitter, twitter)
    
def sentiment_analysis(reviews, source):

    #separar dados treinamento e teste
    training_reviews, test_reviews = split_train_test(reviews, 0.9)
    #X-feature y-label
    X_train_text, X_test_text, y_train, y_test = get_classes(training_reviews, test_reviews)
    #aplicar seleção de feature Bag of words
    X_train, X_test = bow(X_train_text, X_test_text)

    file_header(source)
    
    handle_report( nb(X_train, X_test, y_train, y_test), source)
    
    handle_report( multinominal_nb(X_train, X_test, y_train, y_test), source)

    handle_report( svm(X_train, X_test, y_train, y_test), source)

    handle_report( decision_tree(X_train, X_test, y_train, y_test), source)

    handle_report( logistic_regression(X_train, X_test, y_train, y_test), source)

    handle_report( lstm(X_train_text, X_test_text, y_train, y_test), source)

    handle_report( rnn_simple(X_train_text, X_test_text, y_train, y_test), source)

    handle_report( random_forest(X_train, X_test, y_train, y_test), source)
    
    for classifier in algorithms:
        handle_report( bagging(classifier, X_train, X_test, y_train, y_test), source)
    
    with ThreadPoolExecutor() as executor:
        results = [executor.submit(stacking, list(combination), X_train, X_test, y_train, y_test) for combination in stacking_classifiers]
        for f in as_completed(results):
            handle_report( f.result(), source)