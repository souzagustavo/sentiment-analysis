# printa resultados, salva resultados no relatório e salva modelo serializado
from collections import Counter
from sklearn.model_selection import train_test_split
from database.db import countSteamReviews, countTwitterReviews
from file_util.dump import file_report, save_model

#divide os dados em treinamento e teste. 
# O parâmetro reviews representa uma lista de textos.
# O parâmetro train_percent representa a porcentagem de trainamento. Valor entre 0 e 1. train_percent=0.9 representa 90%.
def split_train_test(reviews, train_percent):
    train_data, test_data = train_test_split(reviews, train_size=train_percent)
    print(f'Training: {len(train_data)} | Test: {len(test_data)}')
    return train_data, test_data

#X feature | y label
def get_classes(training_reviews, test_reviews):
    #para a análise será necessário apenas o texto pré-processado (feature) e a polaridade(label)
    X_train = [review.processedText for review in training_reviews]
    X_test = [review.processedText for review in test_reviews]
    y_train = [review.polarity for review in training_reviews]
    y_test = [review.polarity for review in test_reviews]

    print('Traning_data: ', Counter([polarity for polarity in y_train]))
    print('Test_data: ', Counter([polarity for polarity in y_test]))
    return X_train, X_test, y_train, y_test

# Total de reviews da classe com menos registros na base.
# Conta o total de positivos e total de negativos tanto do Twitter quanto Steam.
# O menor valor será utilizado para balancear os dados do treinamento e avaliação
def get_balance_data(client):
    counters = list()
    counters.append( countTwitterReviews(client, True))
    counters.append( countTwitterReviews(client, False))
    counters.append( countSteamReviews(client, True))
    counters.append( countSteamReviews(client, False))
    print('Polarity Counter of each source: ', counters)
    lowest = min(counters)
    print('Balancing dataset data using the lowest counter: ', lowest)
    return lowest

# mostra no console os relatórios
def show_report(report, source):
    print("=============================\n")
    print(f'Data source: {source} | Classifier: {report.name}\n')
    report.cv_score_format()
    report.pred_score_format()
    print("\n")

def handle_report(report, source):
    show_report(report, source)
    file_report(report.metrics(source), source)
    save_model(report.clf, report.name, source)