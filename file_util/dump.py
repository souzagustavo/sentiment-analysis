import os
import time
from joblib import dump, load

# Caminho do diretório no qual serão salvos os modelos serializados e o arquivo de relatório
root = os.path.abspath(os.curdir)
folder_report = f'{root}\\resources\\reports\\{str(round(time.time() * 1000))}'

# Cria o diretório no qual serão salvos os modelos serializados e o arquivo de relatório
def create_report_folder():
    os.makedirs(f'{folder_report}\\twitter')
    os.makedirs(f'{folder_report}\\steam')

def file_header(source):
    header = 'source;algorithm;cv_avg_accuracy;cv_std_accuracy;cv_avg_f1;std_f1;cv_avg_recall;cv_std_recall;cv_avg_precision;cv_std_precision;pred_accuracy\n'
    file_append(header, source)

def file_report(report_line, source):
    file_append(report_line, source)

def file_append(line, source):
    f = open(f'{folder_report}\\{source}\\report.csv', "a")
    f.write(line)
    f.close()

# Para salvar modelo treinado
def save_model(clf, clf_name, source):
    dump(clf, f'{folder_report}\\{source}\\{clf_name}.sav')

# Para carregar modelo salvo
def load_model(clf_name, source):
    return load(f'{folder_report}\\{source}\\{clf_name}.sav')