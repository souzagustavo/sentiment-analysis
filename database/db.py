from model.review import Review
from pymongo import MongoClient
import os

# cria conexão com banco MongoDB
def connect():
    host = os.getenv('DB_HOST')
    user = os.getenv('DB_USER')
    password = os.getenv('DB_PASSWORD')
    return MongoClient(f"mongodb+srv://{user}:{password}@{host}?retryWrites=true&w=majority")

def findAllSteamReviews(client, limit):
    cursorP = findReviews(client, 'Steam', True, limit)
    cursorN = findReviews(client, 'Steam', False, limit)

    reviews = []
    for document in cursorP:
        reviews.append(parse_review(document))
    for document in cursorN:
        reviews.append(parse_review(document))
    return reviews

def findAllTwitterReviews(client, limit):
    cursorP = findReviews(client, 'Twitter', True, limit)
    cursorN = findReviews(client, 'Twitter', False, limit)

    reviews = []
    for document in cursorP:
        reviews.append(parse_review(document))
    for document in cursorN:
        reviews.append(parse_review(document))
    return reviews

def findAllReviewsByPolarityDescription(client, polarity):
    reviews = []
    cursor = get_reviews_collection(client).find({'polarityDescription': polarity})
    for document in cursor:
        reviews.append(parse_review(document))
    return reviews

def findAllReviewsBySourceAndPolarityDescription(client, source, polarity):
    reviews = []
    cursor = get_reviews_collection(client).find({'source': source, 'polarityDescription': polarity})
    for document in cursor:
        reviews.append(parse_review(document))
    return reviews

def findReviews(client, source, polarity, limit):
    return get_reviews_collection(client).find({'source': source, 'polarity': polarity}).limit(limit)

def countTwitterReviews(client, polarity):
    return get_reviews_collection(client).count_documents({'source': 'Twitter', 'polarity': polarity})

def countSteamReviews(client, polarity):
    return get_reviews_collection(client).count_documents({'source': 'Steam', 'polarity': polarity})

def get_reviews_collection(client):
    database = client["univali"]
    return database["reviews"]

def parse_review(document):
    return Review(document['id'], document['timestamp'], document['rawText'], document['processedText'], document['game'], document['source'], document['polarity']) 
